# Yoyo-Weather

Yoyo-weather is REST-based weather application built using Django 
and Open Weather Map API.

It can be used to get the minimum, maximum, average 
and median temperature for a given city and period of time

### Getting started

Yoyo-weather requires at least [Python 3.7](https://www.python.org/downloads/), to run correctly.

To get started with Yoyo-weather, 

```
git clone https://gitlab.com/lzrs/yoyo-weather.git
```
or ssh
```
git clone git@gitlab.com:lzrs/yoyo-weather.git
```

Cd into `yoyo-weather` directory
```
cd yoyo-weather
```
Create a virtual environment
```
virtualenv venv
```
Activate the virtualenv
```
source venv/bin/activate
```
Run the following, to get the server running:
``` bash
pip install -r requirements.txt

python manage.py migrate

python manage.py runserver
```
The server instance running can be accessed through `http://localhost:8000`

### Available Endpoints
`/api/docs/`

_Openapi schema doc for api endpoints_

`/api/locations/{city}/?days={number_of_days}` 

_Gets the minimum, maximum, average and median temperature for a given city and period of time_ 

