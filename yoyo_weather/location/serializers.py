from rest_framework import serializers


class TemperatureResultSerializer(serializers.Serializer):
    maximum = serializers.FloatField(read_only=True)
    minimum = serializers.FloatField(read_only=True)
    average = serializers.FloatField(read_only=True)
    median = serializers.FloatField(read_only=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
