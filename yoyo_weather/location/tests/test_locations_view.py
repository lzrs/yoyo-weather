from django.test.testcases import TestCase
from requests import RequestException
from rest_framework.test import APIRequestFactory

from .. import views
from ..repository.open_weather_map import Coordinates, PeriodTemp


class LocationsViewTestCase(TestCase):
    request_factory = APIRequestFactory()
    city = 'london'

    def setUp(self):
        def fake_weather_api(latitude, longitude, period_forecast=None):
            return [PeriodTemp(min=20, max=30), PeriodTemp(min=10, max=60), PeriodTemp(min=123, max=365)]

        def fake_coordinates(city_name):
            return Coordinates(lat=20, lon=21)

        views.get_coordinates = fake_coordinates
        views.get_open_weather_api_data = fake_weather_api

    def test_locations_weather_returns_200(self):
        url_base_path = '/api/locations/{city}/?days={number_of_days}'
        request = self.request_factory.get(url_base_path.format(city=self.city, number_of_days=4))
        response = views.location_temperatures(request, self.city)
        self.assertEqual(response.status_code, 200)

    def test_locations_weather_returns_in_format(self):
        url_base_path = '/api/locations/{city}/?days={number_of_days}'
        request = self.request_factory.get(url_base_path.format(city=self.city, number_of_days=4))
        response = views.location_temperatures(request, self.city)
        self.assertIn('minimum', response.data)
        self.assertIn('maximum', response.data)
        self.assertIn('average', response.data)
        self.assertIn('median', response.data)

    def test_locations_weather_works_without_number_of_days(self):
        request = self.request_factory.get('/api/locations/{city}/'.format(city=self.city))
        response = views.location_temperatures(request, self.city)
        self.assertIn('minimum', response.data)
        self.assertIn('maximum', response.data)
        self.assertIn('average', response.data)
        self.assertIn('median', response.data)

    def test_locations_weather_bad_request_for_days_not_number(self):
        url_base_path = '/api/locations/{city}/?days={number_of_days}'
        request = self.request_factory.get(url_base_path.format(city=self.city, number_of_days="unknown"))
        response = views.location_temperatures(request, self.city)
        self.assertEqual(response.status_code, 400)

    def test_locations_weather_bad_request_for_days_and_hours_period(self):
        url_base_path = '/api/locations/{city}/?days={number_of_days}&hours={number_of_hours}'
        request = self.request_factory.get(url_base_path.format(city=self.city, number_of_days=2, number_of_hours=1))
        response = views.location_temperatures(request, self.city)
        self.assertEqual(response.status_code, 400)

    def test_locations_weather_not_found_if_get_coordinates_failures(self):
        def fake_coordinates(city_name):
            raise RequestException()
        views.get_coordinates = fake_coordinates
        url_base_path = '/api/locations/{city}/'
        request = self.request_factory.get(url_base_path.format(city=self.city))
        response = views.location_temperatures(request, self.city)
        self.assertEqual(response.status_code, 404)

    def test_locations_weather_not_found_if_get_data_from_public_api_fails(self):
        def fake_weather_api(latitude, longitude, period_forecast=None):
            raise RequestException()
        views.get_open_weather_api_data = fake_weather_api
        url_base_path = '/api/locations/{city}/'
        request = self.request_factory.get(url_base_path.format(city=self.city))
        response = views.location_temperatures(request, self.city)
        self.assertEqual(response.status_code, 404)
