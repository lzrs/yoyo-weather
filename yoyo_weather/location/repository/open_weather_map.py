from dataclasses import dataclass

import requests

APP_ID = "271d1234d3f497eed5b1d80a07b3fcd1"


@dataclass
class PeriodTemp:
    min: float
    max: float

    @property
    def avg(self):
        return (self.min + self.max)/2


@dataclass
class Coordinates:
    lat: float
    lon: float


def get_response(url):
    return requests.get(url)


def get_open_weather_api_data(latitude, longitude, period_forecast=None):
    """
    Gets temperature weather data for a given 'latitude' and 'longitude',
    from OpenWeatherMap's api over a given 'period_forecast'

    For example:
        when 'period_forecast' is 'daily', it gets forecast over the next 7 days

        when 'period_forecast' is 'hourly', it gets forecast over the next 5 hours

    """
    if not period_forecast:
        period_forecast = "daily"
    path = f"https://api.openweathermap.org/data/2.5/onecall?lat={latitude}&lon={longitude}&appid={APP_ID}"

    if period_forecast == "daily":
        exclude = "alerts,hourly,minutely"
        data = get_response(url=f"{path}&exclude={exclude}").json()["daily"]
    elif period_forecast == "hourly":
        exclude = "alerts,daily,minutely"
        data = get_response(url=f"{path}&exclude={exclude}").json()["hourly"]
    else:
        return []

    return list(map(lambda d: PeriodTemp(min=d["temp"]["min"], max=d["temp"]["max"]), data))


def get_coordinates_from_open_weather_api(city_name):
    """
    Gets the latitude/longitude coordinates from OpenWeatherMap's public api,
    for a given 'city_name'
    """
    path = f"https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={APP_ID}"
    json_response = get_response(url=path).json()
    return Coordinates(lat=json_response["coord"]["lat"], lon=json_response["coord"]["lon"])

