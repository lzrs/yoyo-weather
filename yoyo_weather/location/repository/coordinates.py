from yoyo_weather.location.models import City
from .open_weather_map import Coordinates, get_coordinates_from_open_weather_api


def get_coordinates_from_db(city_name):
    """
    Gets latitude/longitude coordinates for a given 'city name'
    from local DB
    """
    city = City.objects.filter(name=city_name)
    if city.exists():
        city_ = city.get()
        return Coordinates(city_.lat, city_.lon)


def get_coordinates(city_name):
    """
    Gets latitude/longitude coordinates for a given 'city name'
    """
    coordinates_from_db = get_coordinates_from_db(city_name)
    if coordinates_from_db:
        return coordinates_from_db
    else:
        coordinates = get_coordinates_from_open_weather_api(city_name)
        City.objects.update_or_create(name=city_name, lat=coordinates.lat, lon=coordinates.lon)
        return coordinates
