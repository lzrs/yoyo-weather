from django.urls import path

from . import views

app_name = 'location'

urlpatterns = [
    path('<str:city>/', views.location_temperatures, name='get_location_temperatures')
]
