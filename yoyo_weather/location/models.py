from django.db import models


class City(models.Model):
    name = models.CharField(max_length=255, unique=True)
    lat = models.FloatField()
    lon = models.FloatField()

    objects = models.Manager()
