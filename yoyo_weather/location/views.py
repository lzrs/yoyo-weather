import statistics

from django.core.exceptions import ObjectDoesNotExist
from requests import RequestException
from rest_framework import status
from rest_framework.decorators import api_view, schema
from rest_framework.response import Response
from rest_framework.schemas.openapi import AutoSchema

from .repository import get_coordinates
from .repository import get_open_weather_api_data
from .serializers import TemperatureResultSerializer


class _LocationTemperatureSchema(AutoSchema):
    def allows_filters(self, path, method):
        return True

    def get_filter_parameters(self, path, method):
        return [
            {
                'name': 'days',
                'required': False,
                'in': 'query',
                'description': 'number of days for forecast',
                'schema': {
                    'type': 'integer',
                },
            },
            {
                'name': 'hours',
                'required': False,
                'in': 'query',
                'description': 'number of hours for forecast',
                'schema': {
                    'type': 'integer',
                },
            },
        ]


@api_view(['GET'])
@schema(_LocationTemperatureSchema())
def location_temperatures(request, city):
    """
    Gets the temperature for a given city.

    The minimum, maximum, average and median temperatures for a city

    Optionally, also gets over a given period of time,
    passed as 'days' or 'hours' query parameter in the URL
    """
    number_of_hours = request.GET.get('hours', None)
    number_of_days = request.GET.get('days', None)
    if number_of_days and number_of_hours:
        return Response(status=status.HTTP_400_BAD_REQUEST, 
                        data={ "message": "Only one period type is supported" })

    try:
        number_of_days = int(number_of_days) if number_of_days else None
    except ValueError:
        return Response(status=status.HTTP_400_BAD_REQUEST, 
                        data= { "message": "'days' param is not a number" })

    try:
        number_of_hours = int(number_of_hours) if number_of_hours else None
    except ValueError:
        return Response(status=status.HTTP_400_BAD_REQUEST, 
                        data={ "message": "'hours' param is not a number" })

    try:
        coordinates = get_coordinates(city_name=city)
    except (RequestException, ObjectDoesNotExist) as failure:
        return Response(status=status.HTTP_404_NOT_FOUND, 
                        data={ "message": f"'{failure}'" })

    try:
        weather_data = get_open_weather_api_data(latitude=coordinates.lat, longitude=coordinates.lon)
    except RequestException as failure:
        return Response(status=status.HTTP_404_NOT_FOUND, 
                        data={ "message": f"{failure}" })
    else:
        filtered_weather_data = weather_data[:number_of_days or number_of_hours]
        max_temps = list(map(lambda fd: fd.max, filtered_weather_data))
        min_temps = list(map(lambda fd: fd.min, filtered_weather_data))

        data = {
            "minimum": min(min_temps),
            "maximum": max(max_temps),
            "average": statistics.mean(map(lambda fd: fd.avg, filtered_weather_data)),
            "median": statistics.median(min_temps + max_temps)
        }
        serializer = TemperatureResultSerializer(instance=data)
        return Response(data=serializer.data)
