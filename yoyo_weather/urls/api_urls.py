from django.urls import path, include

from yoyo_weather.location import urls as location_urls

urlpatterns = [
    path('api/locations/', include((location_urls, 'location-api'))),
]
